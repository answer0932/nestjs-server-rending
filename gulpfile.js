const gulp = require('gulp');
const watch = require('gulp-watch');
const babel = require('gulp-babel');
const plumber = require('gulp-plumber');
const rollup = require('gulp-rollup');
const replace = require('@rollup/plugin-replace');
const fs = require('fs');

const entry = './dist/**/*.js';
const cleanEntry = './dist/config/index.js';


// 开发环境任务
function buildDev() {
    return watch(entry, () => {
        gulp.src(entry)
            .pipe(plumber())
            .pipe(babel({
                babelrc: false,
                plugins: ["@babel/plugin-transform-modules-commonjs"]
            }))
            .pipe(gulp.dest('dist'));
    })
}

// 生产环境任务
function buildProd() {
    return gulp.src(entry)
        .pipe(babel({
            babelrc: false,
            ignore: [cleanEntry],
            plugins: ["@babel/plugin-transform-modules-commonjs"]
        }))
        .pipe(gulp.dest('dist'));
}

function asyncReadControllersFile() {
    return new Promise((resolve) => {
        fs.readdir('./dist/controllers/', (err, data) => {
            const l = data.length - 1;
            const input = [];
            for (let i = l; i >= 0; --i) {
                input.push(`./dist/controllers/${data[i]}/${data[i]}.controller.js`);
            }
            resolve(input);
        })
    })
}

// 流清洗任务
async function buildConfig() {

    const input = await asyncReadControllersFile();

    return gulp.src(entry)
        .pipe(
            rollup({
                input,
                output: {
                    format: 'cjs'
                },
                plugins: [
                    replace({
                        preventAssignment: true,
                        'process.env.NODE_ENV': JSON.stringify('production'),
                    })
                ]
            })
        )
        .pipe(gulp.dest("./dist"))
}

let build = gulp.series(buildDev);

if (process.env.NODE_ENV === 'production') {
    build = gulp.series(buildProd, buildConfig);
}

gulp.task('default', build);