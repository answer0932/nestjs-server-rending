const dotenv = require('dotenv');
dotenv.config('./env');

const { ASSETS_DIRNAME } = process.env;

const { argv } = require('yargs');

const mode = argv.mode || 'development';
const { merge } = require("webpack-merge");
const glob = require("glob");

const Webpack = require("webpack");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const { join } = require("path");

const HTMLAfterPlugin = require("./build/htmlAfterPlugin");
const envConfig = require(`./build/webpack.${mode}.js`);
const files = glob.sync("./views/pages/**/*.js");

const entry = {};
const htmlPlugins = [];

files.forEach(url => {
    const regexp = /([a-zA-Z]+)\.entry\.js/;
    if (regexp.test(url)) {
        const entryKey = RegExp.$1;
        entry[entryKey] = url;
        htmlPlugins.push(
            new HTMLWebpackPlugin({
                filename: join(__dirname, './', `${ASSETS_DIRNAME}/views/pages/${entryKey}/index.html`),
                template: `./views/pages/${entryKey}/index.html`,
                chunks: [entryKey],
                inject: false, // 不需要自动注入
                minify: {
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: false
                }
            })
        )
    }
});

const baseConfig = {
    mode,
    entry,
    devtool: false,
    output: {
        path: join(__dirname, `./${ASSETS_DIRNAME}`)
    },
    optimization: {
        runtimeChunk: 'single',
    },
    resolve: {
        alias: {
            "@": join(__dirname, './views'),
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    // `.swcrc` can be used to configure swc
                    loader: "swc-loader",

                }
            },
            {
                enforce: 'pre',
                test: /\.css$/,
                exclude: /node_modules/,
                loader: "typed-css-modules-loader"
            },
            {
                test: /\.css$/i,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            // modules: true,
                            importLoaders: 1,
                        },
                    },
                    'postcss-loader',
                ],
            },

        ]
    },
    plugins: [

        new Webpack.ProgressPlugin(),

        ...htmlPlugins,

        new HTMLAfterPlugin(),

        new MiniCssExtractPlugin({
            filename: mode
                ? 'views/pages/[name]/[name].css'
                : 'views/pages/[name]/[name].[contenthash:5].css',
            chunkFilename: mode
                ? 'views/pages/[name]/[name].css'
                : 'views/pages/[name]/[name].[contenthash:5].css',
            ignoreOrder: false,
        }),
        // new BundleAnalyzerPlugin()
    ]
}


module.exports = merge(baseConfig, envConfig);