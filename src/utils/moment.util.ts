import Moment from 'moment'

/**
 * @author 艾斯ACE
 * @func localTime
 * @desc 把本地时间转换为时间戳
 */
export const localTime = (localTime) => {
    return Moment(localTime).format('YYYY-MM-DD HH:mm:ss')
}


/**
 * @author 艾斯ACE
 * @func yesterday
 * @desc 昨天日期
 */
export const yesterday = (format) => {
    // return Moment().subtract('days', 1).format(`YYYY-MM-DD${format ? ' HH:mm:ss' : ''}`)
    return {
        start: Moment().subtract(1, 'days').format(`YYYY-MM-DD`),
        end: Moment().subtract(1, 'days').format(`YYYY-MM-DD`)
    }
}

/**
 * @author 艾斯ACE
 * @func today
 * @desc 今天日期
 */
export const today = () => {
    return {
        // start: Moment().subtract('days', 0).format(`YYYY-MM-DD${format ? ' HH:mm:ss' : ''}`),
        // end: Moment().subtract('days', 0).format(`YYYY-MM-DD${format ? ' HH:mm:ss' : ''}`)
        start: Moment().subtract(0, 'days').format(`YYYY-MM-DD`),
        end: Moment().subtract(0, 'days').format(`YYYY-MM-DD`)
    }
}

/**
 * @author 艾斯ACE
 * @func tomorrow
 * @desc 明天日期
 */
export const tomorrow = (format) => {
    return Moment().add(1, 'days').format(`YYYY-MM-DD${format ? ' HH:mm:ss' : ''}`)
}

/**
 * @author 艾斯ACE
 * @func lastWeek
 * @desc 上周
 */
export const lastWeek = () => {
    return {
        start: Moment().week(Moment().week() - 1).startOf('week').format('YYYY-MM-DD'),
        end: Moment().week(Moment().week() - 1).endOf('week').format('YYYY-MM-DD')
    }
}


/**
 * @author 艾斯ACE
 * @func oneWeek
 * @desc 最近一周
 */
export const oneWeek = () => {
    return {
        start: Moment().subtract(6, 'days').format('YYYY-MM-DD'),
        end: Moment().subtract(0, 'days').format('YYYY-MM-DD')
    }
}

/**
 * @author 艾斯ACE
 * @func currentWeek
 * @desc 本周
 */
export const currentWeek = () => {
    return {
        start: Moment().startOf('week').format('YYYY-MM-DD'),
        end: Moment().endOf('week').format('YYYY-MM-DD')
    }
}

/**
 * @author 艾斯ACE
 * @func nextWeek
 * @desc 下周
 */
export const nextWeek = () => {
    return {
        start: Moment().week(Moment().week() + 1).startOf('week').format('YYYY-MM-DD'),
        end: Moment().week(Moment().week() + 1).endOf('week').format('YYYY-MM-DD')
    }
}


/**
 * @author 艾斯ACE
 * @func nextWeek
 * @desc 上月
 */
export const lastMonth = () => {
    return {
        start: Moment().month(Moment().month() - 1).startOf('month').format('YYYY-MM-DD'),
        end: Moment().month(Moment().month() - 1).endOf('month').format('YYYY-MM-DD')
    }
}

/**
 * @author 艾斯ACE
 * @func oneMonth
 * @desc 近一月
 */
export const oneMonth = () => {
    return {
        start: Moment().subtract(29, 'days').format('YYYY-MM-DD'),
        end: Moment().subtract(0, 'days').format('YYYY-MM-DD')
    }
}

/**
 * @author 艾斯ACE
 * @func thisMonthStart
 * @desc 本月
 */
export const currentMonth = () => {
    return {
        start: Moment().startOf('month').format('YYYY-MM-DD'),
        end: Moment().endOf('month').format('YYYY-MM-DD')
    }
}

/**
 * @author 艾斯ACE
 * @func thisMonthStart
 * @desc 下月
 */
export const nextMonth = () => {
    return {
        start: Moment().startOf('month').subtract(-1, 'month').format('YYYY-MM-DD'),
        end: Moment().endOf('month').subtract(-1, 'month').format('YYYY-MM-DD')
    }
}


/**
 * @author 艾斯ACE
 * @func thisThreeMonthStart
 * @desc 当前三个月
 */

export const threeMonth = () => {
    return {
        start: Moment().subtract(3, 'months').format('YYYY-MM-DD HH:mm:ss'),
        end: Moment().subtract(0, 'months').format('YYYY-MM-DD HH:mm:ss')
    }
}

/**
 * @author 艾斯ACE
 * @func year
 * @desc 本年
 */

export const year = () => {
    return {
        start: Moment().startOf('year').format('YYYY-MM-DD HH:mm:ss'),
        end: Moment().endOf('year').format('YYYY-MM-DD HH:mm:ss')
    }
}