import { load } from 'cheerio';
import { Response } from 'express';

type paramsTypes = {
    response: Response,
    html: any,
    name: string
}

export default async ({ response, html, name }: paramsTypes) => {
    return new Promise(async (resolve, reject) => {
        const $ = load(html);

        $(name).each(function () {
            response.write(`<section id="${name}">${$(this).html()}</section>`, 'utf-8');
        });

        $('.lazyload-js').each(function () {
            response.write(`<script src="${$(this).attr('src')}"></script>`);
        });

        response.end();

    });
}