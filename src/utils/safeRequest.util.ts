import axios from 'axios';

class SafeRequest {
    static fetch(url) {
        return new Promise<string>(async (resolve, reject) => {
            try {
                const { data } = await axios(url);
                resolve(data);
            } catch (error) {
                resolve(error);
            }
        })
    }
}

export default SafeRequest;