import { Module, MiddlewareConsumer, NestModule } from '@nestjs/common';
import { AppService } from './app.service';
import { AboutController } from './controllers/about/about.controller';
import { HomeController } from './controllers/home/home.controller';
import { ResultsController } from './controllers/results/results.controller';
import { RulesController } from './controllers/rules/rules.controller';
import { FaqsController } from './controllers/faqs/faqs.controller';
import { ContactController } from './controllers/contact/contact.controller';
import { NotFoundMiddleware } from './middleware/not-found.middleware';

@Module({
	imports: [],
	controllers: [AboutController, HomeController, ResultsController, RulesController, FaqsController, ContactController],
	providers: [AppService],
})
export class AppModule implements NestModule {
	configure(consumer: MiddlewareConsumer) {
		consumer
			.apply(NotFoundMiddleware)
			.forRoutes('/')
	}
}
