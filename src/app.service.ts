import { Get, Injectable } from '@nestjs/common';
import getBookList from './models/books';

@Injectable()
export class AppService {
	getHello(): string {
		return 'Hello World!';
	}

	async getBookList() {
		const { data } = await new getBookList().getBookList();

		return data;
	}
}
