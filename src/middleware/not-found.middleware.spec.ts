import { NotFoundMiddleware } from './not-found.middleware';

describe('NotFoundMiddleware', () => {
  it('should be defined', () => {
    expect(new NotFoundMiddleware()).toBeDefined();
  });
});
