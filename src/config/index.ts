import { join } from 'path';
import * as dotenv from 'dotenv';
dotenv.config();

type Config = {
    viewDir?: string,
    assetsDir?: string,
    port?: number,
    noCache?: boolean,
}

// 公共配置
let config: Config = {
    viewDir: join(__dirname, '../..', `assets/views`),
    assetsDir: join(__dirname, '../..', 'assets'),
}

// 开发
if (process.env.NODE_ENV === 'deveopment') {
    const devConfig: Config = {
        port: 3000,
        noCache: true
    }

    config = {
        ...config,
        ...devConfig,
    }
}

if (false) {
    console.log(1);
}

// 生产
if (process.env.NODE_ENV === 'production') {

    const prodConfig: Config = {
        port: 80,
        noCache: false,
    }

    config = {
        ...config,
        ...prodConfig,
    }
}

export default config;