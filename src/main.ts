import * as fs from 'fs';

import config from '@/config';
import '@/utils/log4js'
import { NestFactory, } from '@nestjs/core';
import { AppModule, } from './app.module';
import { NestExpressApplication, ExpressAdapter } from '@nestjs/platform-express';
import * as nunjucks from 'nunjucks';
import * as serveStatic from 'serve-static';
import { join } from 'path';
import * as spdy from 'spdy'
import * as express from 'express';
import { createServer } from 'http';

const { ASSETS_PREFIXNAME, ASSETS_DIRNAME, NEST_HTTP_PORT, NEST_HTTPS_PORT } = process.env;

const httpsOptions = {
	key: fs.readFileSync(__dirname + '/ssl/key.pem'),
	cert: fs.readFileSync(__dirname + '/ssl/cert.pem')
};

async function bootstrap() {

	const expressApp = express();

	const app = await NestFactory.create<NestExpressApplication>(AppModule, new ExpressAdapter(expressApp));

	app.useStaticAssets(config.assetsDir, {
		prefix: ASSETS_PREFIXNAME
	});

	const server = spdy.createServer(httpsOptions, expressApp);

	app.setBaseViewsDir(ASSETS_DIRNAME);

	app.setViewEngine('nunjucks');

	app.set('view engine', '.html');

	console.log(process.env.NODE_ENV, 'process.env.NODE_ENV');

	const URL = process.env.NODE_ENV === "deveopment" ? "views" : `${ASSETS_DIRNAME}/views`;

	nunjucks.configure(URL, {
		autoescape: true,
		express: app,
		noCache: true,
		watch: false
	});

	app.use(`/${ASSETS_PREFIXNAME}`, serveStatic(join(__dirname, `../${ASSETS_DIRNAME}`), {
		maxAge: '1d',
		extensions: ['jpg', 'jpeg', 'png', 'gif'],
	}));

	await app.init();

	createServer(expressApp).listen(NEST_HTTP_PORT, () => {
		console.log(`server is running at http://localhost:${NEST_HTTP_PORT}`)
	});

	await server.listen(NEST_HTTPS_PORT, () => {
		console.log(`server is running at https://localhost:${NEST_HTTPS_PORT}`)
	});

}
bootstrap();
