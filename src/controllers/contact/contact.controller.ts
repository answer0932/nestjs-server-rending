import { Controller, Get, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { render } from 'nunjucks';

import createSSRStreamPromise from '@/utils/createSSRStreamPromise';

@Controller()

export class ContactController {
    @Get('contact')
    async index(@Req() req: Request, @Res() res: Response) {
        const html: any = render('pages/Contact/index.html', {
            name: 'contact',
            data: [
                {
                    id: 1,
                    name: 'ace'
                },
                {
                    id: 2,
                    name: 'sabo'
                },
                {
                    id: 3,
                    name: 'luffy'
                }
            ]
        });
        if (req.headers['x-pjax']) {
            console.log("contact-站内切页");
            await createSSRStreamPromise({
                response: res,
                html,
                name: '#contact__container'
            });
        } else {
            console.log("contact-落地页");
            res.render('pages/Contact/index.html', {
                name: 'about',
                data: [
                    {
                        id: 1,
                        name: 'ace'
                    },
                    {
                        id: 2,
                        name: 'sabo'
                    },
                    {
                        id: 3,
                        name: 'luffy'
                    }
                ]
            })
        }
    }
}
