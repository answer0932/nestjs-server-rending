import { Controller, Get, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { render } from 'nunjucks';

import createSSRStreamPromise from '@/utils/createSSRStreamPromise';

@Controller()

export class HomeController {
    @Get('/')
    async index(@Req() req: Request, @Res() res: Response) {
        const html: any = render('pages/Home/index.html', {
            name: 'Home',
            data: [
                {
                    id: 1,
                    name: 'ace'
                },
                {
                    id: 2,
                    name: 'sabo'
                },
                {
                    id: 3,
                    name: 'luffy'
                }
            ]
        });
        if (req.headers['x-pjax']) {
            console.log("home-站内切页");
            await createSSRStreamPromise({
                response: res,
                html,
                name: '#home__container'
            });
        } else {
            console.log("home-落地页");
            res.render('pages/Home/index.html', {
                name: 'HOME',
                data: [
                    {
                        id: 1,
                        name: 'ace'
                    },
                    {
                        id: 2,
                        name: 'sabo'
                    },
                    {
                        id: 3,
                        name: 'luffy'
                    }
                ]
            })
        }
    }
}
