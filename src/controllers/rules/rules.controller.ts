import { Controller, Get, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { render } from 'nunjucks';

import createSSRStreamPromise from '@/utils/createSSRStreamPromise';

@Controller()

export class RulesController {
    @Get('rules')
    async index(@Req() req: Request, @Res() res: Response) {
        const html: any = render('pages/Rules/index.html', {
            name: 'rules',
            data: [
                {
                    id: 1,
                    name: 'ace'
                },
                {
                    id: 2,
                    name: 'sabo'
                },
                {
                    id: 3,
                    name: 'luffy'
                }
            ]
        });
        if (req.headers['x-pjax']) {
            console.log("rules-站内切页");
            await createSSRStreamPromise({
                response: res,
                html,
                name: '#rules__container'
            });
        } else {
            console.log("rules-落地页");
            res.render('pages/Rules/index.html', {
                name: 'about',
                data: [
                    {
                        id: 1,
                        name: 'ace'
                    },
                    {
                        id: 2,
                        name: 'sabo'
                    },
                    {
                        id: 3,
                        name: 'luffy'
                    }
                ]
            })
        }
    }
}
