FROM  keymetrics/pm2:latest-alpine
ADD . /docker/site1
COPY . /site1
WORKDIR /docker/site1
RUN npm config set registry https://registry.npm.taobao.org/ && \ 
    npm install
EXPOSE 3000
CMD ["pm2-runtime", 'start', "process.yaml"]