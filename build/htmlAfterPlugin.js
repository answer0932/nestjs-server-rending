require('dotenv').config('./env');

const { ASSETS_PREFIXNAME } = process.env;

const pluginName = 'ConsoleLogOnBuildWebpackPlugin';
const HTMLWebpackPlugin = require("html-webpack-plugin");
/**
 * @param { compiler }  webpack提供的api
 * @param { compilation } 打包之后的源文件在哪 每个chunk都有一个compilation
 * @desc compiler + compilation 承载了webpack的一切了
 */

const assetHelp = data => {
    let js = [];
    let css = [];
    for (let item of data.js) {
        js.push(`<script class="lazyload-js" src="${item.replace('../../..', ASSETS_PREFIXNAME)}"></script>`);
    }

    return {
        js,
        css
    }
}

class ConsoleLogOnBuildWebpackPlugin {
    constructor() {
        this.jsArr = [];
        this.cssArr = [];
    }

    apply(compiler) {
        compiler.hooks.compilation.tap(pluginName, (compilation) => {
            console.log('webpack 构建正在启动！');
            HTMLWebpackPlugin.getHooks(compilation).beforeEmit.tapAsync(
                pluginName,
                (data, cb) => {
                    let _html = data.html;
                    _html = _html.replace("<!--injectJS-->", this.jsArr.join('')).replace("@/layouts", "../../layouts");
                    _html = _html.replace("<!--injectCSS-->", 2222);
                    data.html = _html;
                    cb(null, data);
                }
            )

            HTMLWebpackPlugin.getHooks(compilation).beforeAssetTagGeneration.tapAsync(
                pluginName,
                (data, cb) => {
                    const { js } = assetHelp(data.assets);
                    this.jsArr = js;
                    cb(null, data)
                }
            )
        });
    }
}

module.exports = ConsoleLogOnBuildWebpackPlugin;