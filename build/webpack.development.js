console.log('开发环境');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { join } = require("path");
const { ASSETS_DIRNAME } = process.env;

module.exports = {
    output: {
        filename: "views/pages/[name]/[name].bundle.js" //分区打包
    },
    plugins: [
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: join(__dirname, '../', 'views/components'),
                    to: join(__dirname, '../', `${ASSETS_DIRNAME}/views/components`),
                    filter: url => {
                        // if (/\.(js|css)$/.test(url)) {
                        //     return false;
                        // }
                        return true
                    }
                },
                {
                    from: join(__dirname, '../', 'views/layouts'),
                    to: join(__dirname, '../', `${ASSETS_DIRNAME}/views/layouts`),
                },
                {
                    from: join(__dirname, '../', '.env'),
                    to: join(__dirname, '../', `${ASSETS_DIRNAME}`),
                },
                {
                    from: join(__dirname, '../', 'ssl'),
                    to: join(__dirname, '../', `${ASSETS_DIRNAME}/ssl`),
                },
            ],
        }),
    ]
}