console.log('生产环境');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { join } = require("path");
const { ASSETS_DIRNAME } = process.env;
const minifier = require('html-minifier').minify;

module.exports = {
    output: {
        filename: "views/pages/[name]/[name].[contenthash:5].bundle.js" //分区打包
    },
    plugins: [
        new UglifyJsPlugin({
            uglifyOptions: {
                compress: {
                    // drop_console: true
                }
            }
        }),

        new CopyWebpackPlugin({
            patterns: [
                {
                    from: join(__dirname, '../', 'views/components'),
                    to: join(__dirname, '../', `${ASSETS_DIRNAME}/views/components`),
                    filter: url => {
                        // if (/\.(js|css)$/.test(url)) {
                        //     return false;
                        // }
                        return true
                    },
                    transform(content) {
                        return minifier(content.toString("utf8"), {
                            collapseWhitespace: true,
                        });
                    }
                },
                {
                    from: join(__dirname, '../', 'views/layouts'),
                    to: join(__dirname, '../', `${ASSETS_DIRNAME}/views/layouts`),
                    transform(content) {
                        return minifier(content.toString("utf8"), {
                            collapseWhitespace: true,
                        });
                    }
                },
                {
                    from: join(__dirname, '../', '.env'),
                    to: join(__dirname, '../', `${ASSETS_DIRNAME}`),
                },
                {
                    from: join(__dirname, '../', 'ssl'),
                    to: join(__dirname, '../', `${ASSETS_DIRNAME}/ssl`),
                },
            ],
        }),
    ]
}