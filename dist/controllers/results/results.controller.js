'use strict';

var __decorate = function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var __metadata = function (k, v) {
  if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var __param = function (paramIndex, decorator) {
  return function (target, key) {
    decorator(target, key, paramIndex);
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ResultsController = void 0;

const common_1 = require("@nestjs/common");

const nunjucks_1 = require("nunjucks");

const createSSRStreamPromise_1 = require("../../utils/createSSRStreamPromise");

let ResultsController = class ResultsController {
  async index(req, res) {
    const html = nunjucks_1.render('pages/Results/index.html', {
      name: 'results',
      data: [{
        id: 1,
        name: 'ace'
      }, {
        id: 2,
        name: 'sabo'
      }, {
        id: 3,
        name: 'luffy'
      }]
    });

    if (req.headers['x-pjax']) {
      console.log("results-站内切页");
      await createSSRStreamPromise_1.default({
        response: res,
        html,
        name: '#results__container'
      });
    } else {
      console.log("results-落地页");
      res.render('pages/Results/index.html', {
        name: 'about',
        data: [{
          id: 1,
          name: 'ace'
        }, {
          id: 2,
          name: 'sabo'
        }, {
          id: 3,
          name: 'luffy'
        }]
      });
    }
  }

};

__decorate([common_1.Get('results'), __param(0, common_1.Req()), __param(1, common_1.Res()), __metadata("design:type", Function), __metadata("design:paramtypes", [Object, Object]), __metadata("design:returntype", Promise)], ResultsController.prototype, "index", null);

ResultsController = __decorate([common_1.Controller()], ResultsController);
exports.ResultsController = ResultsController;
