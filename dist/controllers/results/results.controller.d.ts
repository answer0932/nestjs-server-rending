import { Request, Response } from 'express';
export declare class ResultsController {
    index(req: Request, res: Response): Promise<void>;
}
