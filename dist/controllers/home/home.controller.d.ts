import { Request, Response } from 'express';
export declare class HomeController {
    index(req: Request, res: Response): Promise<void>;
}
