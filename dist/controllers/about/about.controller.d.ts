import { Request, Response } from 'express';
export declare class AboutController {
    index(req: Request, res: Response): Promise<void>;
}
