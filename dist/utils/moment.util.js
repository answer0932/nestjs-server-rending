"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.year = exports.threeMonth = exports.nextMonth = exports.currentMonth = exports.oneMonth = exports.lastMonth = exports.nextWeek = exports.currentWeek = exports.oneWeek = exports.lastWeek = exports.tomorrow = exports.today = exports.yesterday = exports.localTime = void 0;

const moment_1 = require("moment");

const localTime = localTime => {
  return moment_1.default(localTime).format('YYYY-MM-DD HH:mm:ss');
};

exports.localTime = localTime;

const yesterday = format => {
  return {
    start: moment_1.default().subtract(1, 'days').format(`YYYY-MM-DD`),
    end: moment_1.default().subtract(1, 'days').format(`YYYY-MM-DD`)
  };
};

exports.yesterday = yesterday;

const today = () => {
  return {
    start: moment_1.default().subtract(0, 'days').format(`YYYY-MM-DD`),
    end: moment_1.default().subtract(0, 'days').format(`YYYY-MM-DD`)
  };
};

exports.today = today;

const tomorrow = format => {
  return moment_1.default().add(1, 'days').format(`YYYY-MM-DD${format ? ' HH:mm:ss' : ''}`);
};

exports.tomorrow = tomorrow;

const lastWeek = () => {
  return {
    start: moment_1.default().week(moment_1.default().week() - 1).startOf('week').format('YYYY-MM-DD'),
    end: moment_1.default().week(moment_1.default().week() - 1).endOf('week').format('YYYY-MM-DD')
  };
};

exports.lastWeek = lastWeek;

const oneWeek = () => {
  return {
    start: moment_1.default().subtract(6, 'days').format('YYYY-MM-DD'),
    end: moment_1.default().subtract(0, 'days').format('YYYY-MM-DD')
  };
};

exports.oneWeek = oneWeek;

const currentWeek = () => {
  return {
    start: moment_1.default().startOf('week').format('YYYY-MM-DD'),
    end: moment_1.default().endOf('week').format('YYYY-MM-DD')
  };
};

exports.currentWeek = currentWeek;

const nextWeek = () => {
  return {
    start: moment_1.default().week(moment_1.default().week() + 1).startOf('week').format('YYYY-MM-DD'),
    end: moment_1.default().week(moment_1.default().week() + 1).endOf('week').format('YYYY-MM-DD')
  };
};

exports.nextWeek = nextWeek;

const lastMonth = () => {
  return {
    start: moment_1.default().month(moment_1.default().month() - 1).startOf('month').format('YYYY-MM-DD'),
    end: moment_1.default().month(moment_1.default().month() - 1).endOf('month').format('YYYY-MM-DD')
  };
};

exports.lastMonth = lastMonth;

const oneMonth = () => {
  return {
    start: moment_1.default().subtract(29, 'days').format('YYYY-MM-DD'),
    end: moment_1.default().subtract(0, 'days').format('YYYY-MM-DD')
  };
};

exports.oneMonth = oneMonth;

const currentMonth = () => {
  return {
    start: moment_1.default().startOf('month').format('YYYY-MM-DD'),
    end: moment_1.default().endOf('month').format('YYYY-MM-DD')
  };
};

exports.currentMonth = currentMonth;

const nextMonth = () => {
  return {
    start: moment_1.default().startOf('month').subtract(-1, 'month').format('YYYY-MM-DD'),
    end: moment_1.default().endOf('month').subtract(-1, 'month').format('YYYY-MM-DD')
  };
};

exports.nextMonth = nextMonth;

const threeMonth = () => {
  return {
    start: moment_1.default().subtract(3, 'months').format('YYYY-MM-DD HH:mm:ss'),
    end: moment_1.default().subtract(0, 'months').format('YYYY-MM-DD HH:mm:ss')
  };
};

exports.threeMonth = threeMonth;

const year = () => {
  return {
    start: moment_1.default().startOf('year').format('YYYY-MM-DD HH:mm:ss'),
    end: moment_1.default().endOf('year').format('YYYY-MM-DD HH:mm:ss')
  };
};

exports.year = year;