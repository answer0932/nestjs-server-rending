"use strict";

const playwright = require('playwright');

(async () => {
  for (const browserType of ['chromium']) {
    const browser = await playwright[browserType].launch();
    const context = await browser.newContext();
    const page = await context.newPage();
    await page.goto('http://localhost:3000/');
    await page.screenshot({
      path: `./screenshot/example-${browserType}.png`
    });
    await browser.close();
  }
})();

module.exports = playwright;