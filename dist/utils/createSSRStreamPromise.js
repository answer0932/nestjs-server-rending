"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

const cheerio_1 = require("cheerio");

exports.default = async ({
  response,
  html,
  name
}) => {
  return new Promise(async (resolve, reject) => {
    const $ = cheerio_1.load(html);
    $(name).each(function () {
      response.write(`<section id="${name}">${$(this).html()}</section>`, 'utf-8');
    });
    $('.lazyload-js').each(function () {
      response.write(`<script src="${$(this).attr('src')}"></script>`);
    });
    response.end();
  });
};