import { Response } from 'express';
declare type paramsTypes = {
    response: Response;
    html: any;
    name: string;
};
declare const _default: ({ response, html, name }: paramsTypes) => Promise<unknown>;
export default _default;
