declare class SafeRequest {
    static fetch(url: any): Promise<string>;
}
export default SafeRequest;
