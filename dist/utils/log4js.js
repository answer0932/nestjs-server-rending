"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

const log4js = require("log4js");

log4js.configure({
  appenders: {
    globalError: {
      type: "file",
      filename: "./logs/error.log"
    }
  },
  categories: {
    default: {
      appenders: ["globalError"],
      level: "trace"
    }
  }
});
const logger = log4js.getLogger("global");
logger.trace("查出代码");
logger.debug("调试");
logger.info("消息代码");
logger.warn("警告代码");
logger.error("错误代码");
logger.fatal("致命错误");