export declare const localTime: (localTime: any) => string;
export declare const yesterday: (format: any) => {
    start: string;
    end: string;
};
export declare const today: () => {
    start: string;
    end: string;
};
export declare const tomorrow: (format: any) => string;
export declare const lastWeek: () => {
    start: string;
    end: string;
};
export declare const oneWeek: () => {
    start: string;
    end: string;
};
export declare const currentWeek: () => {
    start: string;
    end: string;
};
export declare const nextWeek: () => {
    start: string;
    end: string;
};
export declare const lastMonth: () => {
    start: string;
    end: string;
};
export declare const oneMonth: () => {
    start: string;
    end: string;
};
export declare const currentMonth: () => {
    start: string;
    end: string;
};
export declare const nextMonth: () => {
    start: string;
    end: string;
};
export declare const threeMonth: () => {
    start: string;
    end: string;
};
export declare const year: () => {
    start: string;
    end: string;
};
