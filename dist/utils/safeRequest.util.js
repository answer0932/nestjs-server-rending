"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

const axios_1 = require("axios");

class SafeRequest {
  static fetch(url) {
    return new Promise(async (resolve, reject) => {
      try {
        const {
          data
        } = await axios_1.default(url);
        resolve(data);
      } catch (error) {
        resolve(error);
      }
    });
  }

}

exports.default = SafeRequest;