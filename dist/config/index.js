"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const dotenv = require("dotenv");
dotenv.config();
let config = {
    viewDir: path_1.join(__dirname, '../..', `assets/views`),
    assetsDir: path_1.join(__dirname, '../..', 'assets'),
};
if (process.env.NODE_ENV === 'deveopment') {
    const devConfig = {
        port: 3000,
        noCache: true
    };
    config = Object.assign(Object.assign({}, config), devConfig);
}
if (false) {
    console.log(1);
}
if (process.env.NODE_ENV === 'production') {
    const prodConfig = {
        port: 80,
        noCache: false,
    };
    config = Object.assign(Object.assign({}, config), prodConfig);
}
exports.default = config;
//# sourceMappingURL=index.js.map