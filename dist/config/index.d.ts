declare type Config = {
    viewDir?: string;
    assetsDir?: string;
    port?: number;
    noCache?: boolean;
};
declare let config: Config;
export default config;
