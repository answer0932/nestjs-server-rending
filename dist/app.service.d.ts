export declare class AppService {
    getHello(): string;
    getBookList(): Promise<{
        name: {
            author: string;
            create_date: string;
            id: number;
            isbn: string;
            memo: string;
            name: string;
            price: string;
        }[];
    }>;
}
