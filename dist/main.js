"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

const fs = require("fs");

const config_1 = require("./config");

require("./utils/log4js");

const core_1 = require("@nestjs/core");

const app_module_1 = require("./app.module");

const platform_express_1 = require("@nestjs/platform-express");

const nunjucks = require("nunjucks");

const serveStatic = require("serve-static");

const path_1 = require("path");

const spdy = require("spdy");

const express = require("express");

const http_1 = require("http");

const {
  ASSETS_PREFIXNAME,
  ASSETS_DIRNAME,
  NEST_HTTP_PORT,
  NEST_HTTPS_PORT
} = process.env;
const httpsOptions = {
  key: fs.readFileSync(__dirname + '/ssl/key.pem'),
  cert: fs.readFileSync(__dirname + '/ssl/cert.pem')
};

async function bootstrap() {
  const expressApp = express();
  const app = await core_1.NestFactory.create(app_module_1.AppModule, new platform_express_1.ExpressAdapter(expressApp));
  app.useStaticAssets(config_1.default.assetsDir, {
    prefix: ASSETS_PREFIXNAME
  });
  const server = spdy.createServer(httpsOptions, expressApp);
  app.setBaseViewsDir(ASSETS_DIRNAME);
  app.setViewEngine('nunjucks');
  app.set('view engine', '.html');
  console.log(process.env.NODE_ENV, 'process.env.NODE_ENV');
  const URL = process.env.NODE_ENV === "deveopment" ? "views" : `${ASSETS_DIRNAME}/views`;
  nunjucks.configure(URL, {
    autoescape: true,
    express: app,
    noCache: true,
    watch: false
  });
  app.use(`/${ASSETS_PREFIXNAME}`, serveStatic(path_1.join(__dirname, `../${ASSETS_DIRNAME}`), {
    maxAge: '1d',
    extensions: ['jpg', 'jpeg', 'png', 'gif']
  }));
  await app.init();
  http_1.createServer(expressApp).listen(NEST_HTTP_PORT, () => {
    console.log(`server is running at http://localhost:${NEST_HTTP_PORT}`);
  });
  await server.listen(NEST_HTTPS_PORT, () => {
    console.log(`server is running at https://localhost:${NEST_HTTPS_PORT}`);
  });
}

bootstrap();