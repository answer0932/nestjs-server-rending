"use strict";

var __decorate = void 0 && (void 0).__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppModule = void 0;

const common_1 = require("@nestjs/common");

const app_service_1 = require("./app.service");

const about_controller_1 = require("./controllers/about/about.controller");

const home_controller_1 = require("./controllers/home/home.controller");

const results_controller_1 = require("./controllers/results/results.controller");

const rules_controller_1 = require("./controllers/rules/rules.controller");

const faqs_controller_1 = require("./controllers/faqs/faqs.controller");

const contact_controller_1 = require("./controllers/contact/contact.controller");

const not_found_middleware_1 = require("./middleware/not-found.middleware");

let AppModule = class AppModule {
  configure(consumer) {
    consumer.apply(not_found_middleware_1.NotFoundMiddleware).forRoutes('/');
  }

};
AppModule = __decorate([common_1.Module({
  imports: [],
  controllers: [about_controller_1.AboutController, home_controller_1.HomeController, results_controller_1.ResultsController, rules_controller_1.RulesController, faqs_controller_1.FaqsController, contact_controller_1.ContactController],
  providers: [app_service_1.AppService]
})], AppModule);
exports.AppModule = AppModule;