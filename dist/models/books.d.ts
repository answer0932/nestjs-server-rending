declare class BookList {
    getBookList(): {
        data: {
            name: {
                author: string;
                create_date: string;
                id: number;
                isbn: string;
                memo: string;
                name: string;
                price: string;
            }[];
        };
    };
}
export default BookList;
