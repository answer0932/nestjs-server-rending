"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

class BookList {
  getBookList() {
    return {
      data: {
        "name": [{
          "author": "罗贯中",
          "create_date": "2021-08-07 19:10:51",
          "id": 4,
          "isbn": "",
          "memo": "罗贯中的书",
          "name": "三国演义",
          "price": "2.00"
        }, {
          "author": "吴承恩",
          "create_date": "2021-07-13 22:42:24",
          "id": 3,
          "isbn": "",
          "memo": "吴承恩的书",
          "name": "西游记",
          "price": "1.00"
        }, {
          "author": "施耐庵",
          "create_date": "2021-08-07 19:11:44",
          "id": 5,
          "isbn": "",
          "memo": "施耐庵的书",
          "name": "水浒传",
          "price": "3.00"
        }, {
          "author": "曹雪芹",
          "create_date": "2021-08-07 19:12:17",
          "id": 6,
          "isbn": "",
          "memo": "曹雪芹的书",
          "name": "红楼梦",
          "price": "5.00"
        }]
      }
    };
  }

}

exports.default = BookList;